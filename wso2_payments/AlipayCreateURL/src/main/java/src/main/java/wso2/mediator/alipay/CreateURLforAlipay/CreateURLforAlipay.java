package src.main.java.wso2.mediator.alipay.CreateURLforAlipay;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.ManagedLifecycle;
import org.apache.synapse.MessageContext;
import org.apache.synapse.core.SynapseEnvironment;
import org.apache.synapse.mediators.AbstractMediator;



import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.net.URLEncoder;

public class CreateURLforAlipay extends AbstractMediator implements ManagedLifecycle {


    /**
     * Used building output as Hex
     */
    private static final char[] DIGITS = { '0', '1', '2', '3', '4', '5', '6',
            '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    public String alipayURL ;
    
        
    public String getAlipayURL() {
		return alipayURL;
	}

	public void setAlipayURL(String alipayURL) {
		this.alipayURL = alipayURL;
	}


	private static final Log log = LogFactory.getLog(CreateURLforAlipay.class);

    @Override
    public boolean mediate(MessageContext messageContext) {

        if(log.isDebugEnabled()){
            log.info("MD5Mediator mediator is invoked");
        }

        String gateWayUrl = String.valueOf(messageContext.getProperty("gateWayUrl"));
        String key = String.valueOf(messageContext.getProperty("key"));
        String _input_charset = String.valueOf(messageContext.getProperty("_input_charset"));
      
       Hashtable<String, String> param = new Hashtable<String, String>();
       param.put("service", String.valueOf(messageContext.getProperty("service")));
        param.put("partner", String.valueOf(messageContext.getProperty("partner")));
       param.put("logon_id", String.valueOf(messageContext.getProperty("logon_id")));
       param.put("page_no", String.valueOf(messageContext.getProperty("page_no")));
     param.put("page_size", String.valueOf(messageContext.getProperty("page_size")));
     param.put("gmt_end_time", String.valueOf(messageContext.getProperty("gmt_end_time")));
     param.put("gmt_start_time", String.valueOf(messageContext.getProperty("gmt_start_time")));
     param.put("sendFormat", String.valueOf(messageContext.getProperty("sendFormat")));
     
          ////////////////////////////////////////////////////////////////////

        //  processing logic
        String url = getURL(gateWayUrl, param, key, _input_charset);


        // set Url for send mediator
        
        messageContext.setProperty("alipayURL", url);

        messageContext.setTo(new EndpointReference(url));
        return true;
    }

    @Override
    public void init(SynapseEnvironment synapseEnvironment) {
        // ignore
    }

    @Override
    public void destroy() {
        // ignore
    }


    //Date transformation Logic
    public Date date_conversion(int minutes, String startDate, String dateFormat) throws ParseException {

        final String DATE_FORMAT = dateFormat;
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);

        Date dateObj = df.parse(startDate);
        Calendar dateCal =Calendar.getInstance();
        dateCal.setTime(dateObj);
        dateCal.add(dateCal.MINUTE,minutes);
        Date endDate =  dateCal.getTime();

        return endDate;

    }


    public String md5(String text) {
        MessageDigest msgDigest = null;

        try {
            msgDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(
                    "System doesn't support MD5 algorithm.");
        }

        try {
            msgDigest.update(text.getBytes("GBK"));

        } catch (UnsupportedEncodingException e) {

            throw new IllegalStateException(
                    "System doesn't support your  EncodingException.");

        }

        byte[] bytes = msgDigest.digest();

        String md5Str = new String(encodeHex(bytes));

        return md5Str;
    }

    public char[] encodeHex(byte[] data) {

        int l = data.length;

        char[] out = new char[l << 1];

        // two characters form the hex value.
        for (int i = 0, j = 0; i < l; i++) {
            out[j++] = DIGITS[(0xF0 & data[i]) >>> 4];
            out[j++] = DIGITS[0x0F & data[i]];
        }

        return out;
    }


    public String sign(String content, String privateKey) {
        if (privateKey == null) {
            return null;
        }
        String signBefore = content + privateKey;

        //*********************************************************************
        return md5(signBefore);

    }


    private String getContent(Map params, String privateKey) {
        List keys = new ArrayList(params.keySet());
        Collections.sort(keys);

        String prestr = "";

        for (int i = 0; i < keys.size(); i++) {
            String key = (String) keys.get(i);
            String value = (String) params.get(key);

            if (i == keys.size() - 1) {
                prestr = prestr + key + "=" + value;
            } else {
                prestr = prestr + key + "=" + value + "&";
            }
        }


        return prestr + privateKey;
    }


    public String getURL(String gatewayURL,
                                Hashtable params,
                                String key,String charset) {

        String sign = md5(getContent(params, key));

        String parameter = gatewayURL;

        List keys = new ArrayList(params.keySet());
        for (int i = 0; i < keys.size(); i++) {
            try {
                parameter = parameter + keys.get(i) + "="
                        + URLEncoder.encode((String) params.get(keys.get(i)), charset) + "&";


            } catch (UnsupportedEncodingException e) {

                e.printStackTrace();
            }
        }
        parameter = parameter + "sign=" + sign ;

        return parameter;
    }

}
