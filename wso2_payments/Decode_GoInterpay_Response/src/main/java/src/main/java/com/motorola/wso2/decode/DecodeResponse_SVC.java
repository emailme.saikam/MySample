package src.main.java.com.motorola.wso2.decode;

import java.io.UnsupportedEncodingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;
import java.net.URLDecoder;
 
public class DecodeResponse_SVC extends AbstractMediator { 

    private Log log = LogFactory.getLog(DecodeResponse_SVC.class);

    public boolean mediate(MessageContext messageContext) {
        String EncodedString = null;
        String stringProperty = (String) messageContext.getProperty("String");

        if (stringProperty == null) {
            log.error("String property has not been set.");
            return true;
        }

        try {
            EncodedString = URLDecoder.decode(stringProperty, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            handleException("UnsupportEncodingException caught", e, messageContext);
        } catch (Exception e) {
            handleException("Unknown exception caught", e, messageContext);
        }

        messageContext.setProperty("DecodedString", EncodedString);

        return true;
    }

}
