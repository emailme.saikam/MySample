function invoice_IMEI_dss_addEntry_payload(mc) {
    var message_body = '<![CDATA[' + mc.getPayloadXML() + ']]>';
    var message_id = mc.getProperty('MESSAGE_ID');
    var invoice_id = mc.getProperty('INVOICE_ID');
    var entry_type = mc.getProperty('ENTRY_TYPE');
    var destination = mc.getProperty('DESTINATION');
    mc.setPayloadXML(<dat:addEntry xmlns:dat="http://ws.wso2.org/dataservice">
        <dat:message_id>{message_id}</dat:message_id>
        <dat:invoice_id>{invoice_id}</dat:invoice_id>
        <dat:entry_type>{entry_type}</dat:entry_type>
        <dat:destination>{destination}</dat:destination>
        <dat:message_body>{message_body}</dat:message_body>
      </dat:addEntry>);
}