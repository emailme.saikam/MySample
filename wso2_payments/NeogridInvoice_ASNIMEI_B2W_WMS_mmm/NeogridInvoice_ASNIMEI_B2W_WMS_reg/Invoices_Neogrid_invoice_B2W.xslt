<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:param name="destination"/>
<xsl:param name="cdataBody"/>
<xsl:param name="b2wPassword"/>
<xsl:template match="/">
	<restenv>
	    <autenticacao>
		<usuario>WS.MOTOROLA</usuario>
		<senha><xsl:value-of select="$b2wPassword"/></senha>
	    </autenticacao>
	    <parametros>
		<parametro>
		    <p_cnpj_destinatario><xsl:value-of select="$destination"/></p_cnpj_destinatario>
		    <p_xml_sefaz><xsl:value-of select="$cdataBody" disable-output-escaping="yes"/></p_xml_sefaz>
		</parametro>
	    </parametros>
	</restenv>
</xsl:template>
</xsl:stylesheet>
