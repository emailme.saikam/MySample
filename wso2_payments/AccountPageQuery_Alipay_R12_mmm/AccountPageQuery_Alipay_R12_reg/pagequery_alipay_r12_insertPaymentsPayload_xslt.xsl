<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
        <xsl:strip-space elements="*"/>
        <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
        <xsl:template match="/">
            <xs:_post_insertpayments_batch_req xmlns:xs="http://ws.wso2.org/dataservice">
                <xsl:for-each xmlns="http://ws.apache.org/ns/synapse" select="//alipay/response/account_page_query_result/account_log_list/AccountQueryAccountLogVO">
                     <_post_insertpayments xmlns="http://ws.wso2.org/dataservice">
                        <xs:trade_no><xsl:value-of select="trade_no"/></xs:trade_no>
                        <xs:merchant_out_order_no><xsl:value-of select="merchant_out_order_no"/></xs:merchant_out_order_no>
						<xs:partner_id><xsl:value-of select="partner_id"/></xs:partner_id>
						<xs:trans_code_msg><xsl:value-of select="trans_code_msg"/></xs:trans_code_msg>
						<xs:trans_date><xsl:value-of select="trans_date"/></xs:trans_date>
                        <xs:currency><xsl:value-of select="currency"/></xs:currency>
						<xs:income><xsl:value-of select="income"/></xs:income>
						<xs:outcome><xsl:value-of select="outcome"/></xs:outcome>
						<xs:balance><xsl:value-of select="balance"/></xs:balance>
                    </_post_insertpayments>
                </xsl:for-each>
            </xs:_post_insertpayments_batch_req>
        </xsl:template>
    </xsl:stylesheet>