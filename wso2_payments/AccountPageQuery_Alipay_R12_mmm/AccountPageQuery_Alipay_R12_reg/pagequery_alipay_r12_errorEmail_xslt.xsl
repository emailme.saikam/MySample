<?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
        <xsl:param name="EmailErrorMessage"/>
		<xsl:param name="SLT_CORRELATION_ID"/>
		<xsl:param name="SLT_INTEGRATION_NAME"/>
		<xsl:param name="SLT_INTEGRATION_STEP"/>
        <xsl:output method="text" encoding="iso-8859-1"/>
        <xsl:template match="/">
          <xsl:text>Hi Team,
			
			AccountPageQuery loading from Alipay to R12, has failed with Correlation ID: </xsl:text><xsl:value-of select="$SLT_CORRELATION_ID"/>
			
            <xsl:text>
            
			Integration Name: </xsl:text><xsl:value-of select="$SLT_INTEGRATION_NAME"/>
			
            <xsl:text>
            
			Integration Step: </xsl:text><xsl:value-of select="$SLT_INTEGRATION_STEP"/>
			
            <xsl:text>
            
			Error Details: </xsl:text><xsl:value-of select="$EmailErrorMessage"/>
			
			<xsl:text>
			Please check ESB-Payments and DSS-R12, Logs for more information.</xsl:text>
            <xsl:text>
 
Best Regards,
Motorola Notifications</xsl:text>
        </xsl:template>
    </xsl:stylesheet>
