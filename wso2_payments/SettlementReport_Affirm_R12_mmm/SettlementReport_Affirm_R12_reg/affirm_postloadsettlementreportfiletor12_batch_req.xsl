<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:strip-space elements="*"/>
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:param name="TODAYS_DATE"/>
<xsl:template match="//SettlementReports">
  <_postloadsettlementreportfiletor12_batch_req>
   <xsl:for-each select="report">
      <_postloadsettlementreportfiletor12>
         <xsl:variable name="eventType"><xsl:value-of select="event_type"/></xsl:variable>
	 	 <PARTNER_TRX_NUM><xsl:value-of select="charge_id"/></PARTNER_TRX_NUM>
         <INTERNAL_REF_NUM><xsl:value-of select="order_id"/></INTERNAL_REF_NUM>
         <TRX_TYPE><xsl:value-of select="event_type"/></TRX_TYPE>
	 	 <TRX_DATE><xsl:value-of select="charge_created_date"/></TRX_DATE>
		 <xsl:choose>
		 <xsl:when test="$eventType='loan_captured'">
		 <TRX_AMOUNT><xsl:value-of select="sales"/></TRX_AMOUNT>
		 </xsl:when>
		 <xsl:otherwise>
		 <TRX_AMOUNT><xsl:value-of select="refunds"/></TRX_AMOUNT>
		 </xsl:otherwise>
		 </xsl:choose>
         <TRX_CHARGE_AMOUNT><xsl:value-of select="fees"/></TRX_CHARGE_AMOUNT>
        <REFUND_REFERENCE_ID><xsl:value-of select="transaction_id"/></REFUND_REFERENCE_ID>
      </_postloadsettlementreportfiletor12>
	  </xsl:for-each>
   </_postloadsettlementreportfiletor12_batch_req>
</xsl:template>
</xsl:stylesheet>