<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:strip-space elements="*"/>
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:param name="TypeID"/>
<xsl:param name="SALES_PROGRAM_NAME"/>
<xsl:param name="LogID"/>
<xsl:param name="COUNTRY_CODE"/>
<xsl:template match="//PriceDetails">
  <PriceDetails>
    <xsl:for-each select="PriceDetail">
      <PriceDetail>
      	<SALES_PROGRAM_ID><xsl:value-of select="SALES_PROGRAM_ID"/></SALES_PROGRAM_ID>
      	<MOTOROLA_ITEM><xsl:value-of select="MOTOROLA_ITEM"/></MOTOROLA_ITEM>
      	<AVAILABLE_QUANTITY><xsl:value-of select="AVAILABLE_QUANTITY"/></AVAILABLE_QUANTITY>
      	<WM_RUN_ID><xsl:value-of select="WM_RUN_ID"/></WM_RUN_ID>
      	<LIST_PRICE><xsl:value-of select="LIST_PRICE"/></LIST_PRICE>
      	<CURRENCY_CODE><xsl:value-of select="CURRENCY_CODE"/></CURRENCY_CODE>
      	<TypeID><xsl:value-of select="$TypeID"/></TypeID>	
      	<SALES_PROGRAM_NAME><xsl:value-of select="$SALES_PROGRAM_NAME"/></SALES_PROGRAM_NAME>	
      	<LogID><xsl:value-of select="$LogID"/></LogID>
      	<COUNTRY_CODE><xsl:value-of select="$COUNTRY_CODE"/></COUNTRY_CODE>	            
      </PriceDetail>
    </xsl:for-each>
  </PriceDetails>
</xsl:template>
</xsl:stylesheet>