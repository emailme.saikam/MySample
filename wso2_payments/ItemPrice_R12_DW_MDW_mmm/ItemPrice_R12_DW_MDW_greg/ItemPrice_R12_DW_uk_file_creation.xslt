<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:template match="/">
      <ns:pricebooks xmlns:ns="http://www.demandware.com/xml/impex/pricebook/2006-10-31">
         <ns:pricebook>
            <ns:header pricebook-id="demandware Regular Orders - UK">
               <ns:currency>GBP</ns:currency>
               <ns:display-name>List Prices</ns:display-name>
               <ns:online-flag>true</ns:online-flag>
            </ns:header>
            <ns:price-tables>
               <xsl:for-each select="//PriceDetails/PriceDetail">
                  <ns:price-table>
                     <xsl:attribute name="product_id">
                        <xsl:value-of select="MOTOROLA_ITEM" />
                     </xsl:attribute>
                     <ns:amount>
                        <xsl:attribute name="quantity">
                           <xsl:value-of select="AVAILABLE_QUANTITY" />
                        </xsl:attribute>
                        <xsl:value-of select="LIST_PRICE" />
                     </ns:amount>
                  </ns:price-table>
               </xsl:for-each>
            </ns:price-tables>
         </ns:pricebook>
      </ns:pricebooks>
   </xsl:template>
</xsl:stylesheet>

