<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
        <xsl:strip-space elements="*"/>
        <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
        <xsl:template match="/">
		<detailData><xsl:for-each xmlns="http://ws.apache.org/ns/synapse" xmlns:xs="http://ws.wso2.org/dataservice" select="//*[name()='refunds']/*[name()='refund']"><xsl:value-of select="*[name()='partner_trx_num']"/>^<xsl:value-of select="*[name()='trx_amount']"/>^<xsl:value-of select="*[name()='reason']"/>#</xsl:for-each></detailData></xsl:template></xsl:stylesheet>