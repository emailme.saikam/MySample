package src.main.java.com.motorola.wso2;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

public class CreateURL_SVC extends AbstractMediator { 
    private Log log = LogFactory.getLog(CreateURL_SVC.class);

    public boolean mediate(MessageContext messageContext) {
        String Message = (String) messageContext.getProperty("message");
        String Secret = (String) messageContext.getProperty("secret");

        if (Message == null || Secret == null) {
            log.error("The message or secret property has not been set");
            return true;
        }

        String S = null;
        String FinalParam = null;
        String FinalRequest = null;
        String FinalSign = null;

        Mac sha256_HMAC;
        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(Secret.getBytes(),
                    "HmacSHA256");
            sha256_HMAC.init(secret_key);
            S = new String(DatatypeConverter.printBase64Binary(sha256_HMAC
                    .doFinal(Message.getBytes())));
            FinalSign = URLEncoder.encode(S, "UTF-8");
            FinalRequest = URLEncoder.encode(Message, "UTF-8");
            FinalParam = "request=" + FinalRequest + "&signature=" + FinalSign;

        } catch (NoSuchAlgorithmException e) {
            handleException("NoSuchAlgorithmException caught", e, messageContext);
        } catch (InvalidKeyException e) {
            handleException("InvalidKeyException caught", e, messageContext);
        } catch (UnsupportedEncodingException e) {
            handleException("UnsupportedEncodingException caught", e, messageContext);
        } catch (Exception e) {
            handleException("Unknown exception caught", e, messageContext);
        }

        messageContext.setProperty("Signature", S);
        messageContext.setProperty("EncodedRequest", FinalParam);

        return true;
    }

}
