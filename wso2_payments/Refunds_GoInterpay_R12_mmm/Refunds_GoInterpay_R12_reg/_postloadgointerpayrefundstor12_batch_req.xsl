<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:strip-space elements="*"/>
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:param name="merchantID"/>
<xsl:param name="TODAYS_DATE"/>
<xsl:template match="//refunds">
  <_postloadgointerpayrefundstor12_batch_req>
    <xsl:for-each select="refund">
      <_postloadgointerpayrefundstor12>
      	<PARTNER_TRX_NUM><xsl:value-of select="Checkout_ID"/></PARTNER_TRX_NUM>
      	<INTERNAL_ACCOUNT_ID><xsl:value-of select="$merchantID"/></INTERNAL_ACCOUNT_ID>
      	<TRX_DATE><xsl:value-of select="Refund_Date"/></TRX_DATE>
      	<CURRENCY_CODE><xsl:value-of select="Original_Currency"/></CURRENCY_CODE>
      	<TRX_AMOUNT><xsl:value-of select="(Refund_Amount + Merchant_Markup) * Ref_Rate"/></TRX_AMOUNT>
      	<TRX_CHARGE_AMOUNT><xsl:value-of select="Refund_Fee_Orig_Ccy"/></TRX_CHARGE_AMOUNT>
      	<CREATION_DATE><xsl:value-of select="$TODAYS_DATE"/></CREATION_DATE>
      	<LAST_UPDATE_DATE><xsl:value-of select="$TODAYS_DATE"/></LAST_UPDATE_DATE>
      	<TRX_AMOUNT_SETTLEMENT><xsl:value-of select="Refund_Amount + Merchant_Markup"/></TRX_AMOUNT_SETTLEMENT>
      	<TRX_CHARGE_AMOUNT_SETTLEMENT><xsl:value-of select="Refund_Fee"/></TRX_CHARGE_AMOUNT_SETTLEMENT>
      	<CURRENCY_CODE_SETTLEMENT><xsl:value-of select="Settlement_Currency"/></CURRENCY_CODE_SETTLEMENT>
      	<REFUND_REFERENCE_ID><xsl:value-of select="Merchant_Ref"/></REFUND_REFERENCE_ID>
      	<EXCHANGE_RATE><xsl:value-of select="Ref_Rate"/></EXCHANGE_RATE>
      	<INTERNAL_REF_NUM><xsl:value-of select="Merchant_Order_ID"/></INTERNAL_REF_NUM>
      </_postloadgointerpayrefundstor12>
    </xsl:for-each>
  </_postloadgointerpayrefundstor12_batch_req>
</xsl:template>
</xsl:stylesheet>