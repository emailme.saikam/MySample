<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" />
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="TRX_DATE">
    	<xsl:if test="normalize-space(.) != ''">
	        <xsl:element name="TRX_DATE">
            	<xsl:call-template name="formatDate">
        	        <xsl:with-param name="dateParam" select="." />
    	        </xsl:call-template>
	        </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template name="formatDate">
        <xsl:param name="dateParam" />
       <!--parse out the day, month and year -->
       <xsl:variable name="mmm" select="substring($dateParam,1,2)" />
       <xsl:variable name="day" select="substring($dateParam,4,2)" />

       <xsl:value-of select="concat($day,'-')"/>

	   <xsl:choose>
	     	<xsl:when test="$mmm = 01">JAN</xsl:when>
   	 		<xsl:when test="$mmm = 02">FEB</xsl:when>
    	 	<xsl:when test="$mmm = 03">MAR</xsl:when>
	   	 	<xsl:when test="$mmm = 04">APR</xsl:when>
     		<xsl:when test="$mmm = 05">MAY</xsl:when>
   		 	<xsl:when test="$mmm = 06">JUN</xsl:when>
	     	<xsl:when test="$mmm = 07">JUL</xsl:when>
   	 		<xsl:when test="$mmm = 08">AUG</xsl:when>
    	 	<xsl:when test="$mmm = 09">SEP</xsl:when>
	   	 	<xsl:when test="$mmm = 10">OCT</xsl:when>
			<xsl:when test="$mmm = 11">NOV</xsl:when>
    	 	<xsl:when test="$mmm = 12">DEC</xsl:when>
	   </xsl:choose> 

       <xsl:variable name="year" select="substring($dateParam,7,4)" />              
       <!-- now print them out. Pad with 0 where necessary -->
       
      <xsl:value-of select="concat('-',$year)"/>
   </xsl:template>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>