<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:strip-space elements="*"/>
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:param name="merchantID"/>
<xsl:param name="TODAYS_DATE"/>
<xsl:template match="//Orders">
  <_postloadgointerpayorderstor12_batch_req>
    <xsl:for-each select="order">
      <_postloadgointerpayorderstor12>
         <PARTNER_TRX_NUM><xsl:value-of select="Checkout_ID"/></PARTNER_TRX_NUM>
         <TRX_AMOUNT><xsl:value-of select="(Amount_Product + Amount_Shipping + Amount_Duty + Amount_Taxes + Merchant_Markup) * Txn_Rate"/></TRX_AMOUNT>
         <TRX_DATE><xsl:value-of select="Order_Date"/></TRX_DATE>
         <CURRENCY_CODE><xsl:value-of select="Original_Currency"/></CURRENCY_CODE>
         <TRX_CHARGE_AMOUNT><xsl:value-of select="Processing_Fee_Orig_Ccy + Fraud_Engine_Fee_Orig_Ccy + Financing_Fee_Orig_Ccy"/></TRX_CHARGE_AMOUNT>
         <TRX_AMOUNT_SETTLEMENT><xsl:value-of select="Amount_Product + Amount_Shipping + Amount_Duty + Amount_Taxes + Merchant_Markup"/></TRX_AMOUNT_SETTLEMENT>
         <TRX_CHARGE_AMOUNT_SETTLEMENT><xsl:value-of select="Processing_Fee + Fraud_Engine_Fee + Financing_Fee"/></TRX_CHARGE_AMOUNT_SETTLEMENT>
         <EXCHANGE_RATE><xsl:value-of select="Txn_Rate"/></EXCHANGE_RATE>
         <CURRENCY_CODE_SETTLEMENT><xsl:value-of select="Settlement_Currency"/></CURRENCY_CODE_SETTLEMENT>
         <INTERNAL_REF_NUM><xsl:value-of select="Merchant_Order_ID"/></INTERNAL_REF_NUM>
         <CREATION_DATE><xsl:value-of select="$TODAYS_DATE"/></CREATION_DATE>
         <LAST_UPDATE_DATE><xsl:value-of select="$TODAYS_DATE"/></LAST_UPDATE_DATE>
	     <INTERNAL_ACCOUNT_ID><xsl:value-of select="$merchantID"/></INTERNAL_ACCOUNT_ID>
      </_postloadgointerpayorderstor12>
    </xsl:for-each>
  </_postloadgointerpayorderstor12_batch_req>
</xsl:template>
</xsl:stylesheet>