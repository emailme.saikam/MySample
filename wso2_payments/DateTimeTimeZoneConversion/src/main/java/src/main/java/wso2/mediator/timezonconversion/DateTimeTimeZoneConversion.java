package src.main.java.wso2.mediator.timezonconversion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.synapse.MessageContext; 
import org.apache.synapse.mediators.AbstractMediator;

public class DateTimeTimeZoneConversion extends AbstractMediator { 
	
public String EndDateTime ;
    
    
    public String getEndDateTime() {
		return EndDateTime;
	}

	public void setEndDateTime(String EndDateTime) {
		this.EndDateTime = EndDateTime;
	}

	public boolean mediate(MessageContext messageContext) { 
		
		String sourcedatetime = String.valueOf(messageContext.getProperty("sourcedatetime"));
		String sourceTimeZone =String.valueOf(messageContext.getProperty("sourceTimeZone"));
		String targetTimeZone =String.valueOf(messageContext.getProperty("targetTimeZone"));
		String dateformat =String.valueOf(messageContext.getProperty("dateformat"));
		
		Date targetdatetime;
		DateFormat expireFormat = new SimpleDateFormat(dateformat);

		        try {
		            expireFormat.setTimeZone(TimeZone.getTimeZone(sourceTimeZone));
		            targetdatetime = expireFormat.parse(sourcedatetime);
		            
		            expireFormat.setTimeZone(TimeZone.getTimeZone(TimeZone.getTimeZone(targetTimeZone).getID()));
		            EndDateTime = expireFormat.format(targetdatetime);
		            
		           
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		       
				messageContext.setProperty("EndDateTime", EndDateTime);		
		
		return true;
	}
}
